import * as Phaser from "phaser";
import { ButtonOptions, ButtonTextOptions } from "./buttonOptions";
declare class Button {
    name: string;
    scene: Phaser.Scene;
    pluginManager: Phaser.Plugins.PluginManager;
    private debug;
    onClick: Function | undefined;
    onPointerover: Function | undefined;
    onPointerout: Function | undefined;
    private readonly x;
    private readonly y;
    private text;
    private readonly height;
    private readonly width;
    private readonly borderWidth;
    private readonly borderColor;
    private readonly borderAlpha;
    private readonly hoverColor;
    private readonly backgroundColor;
    private readonly backgroundAssetKey;
    private readonly backgroundAssetPlay;
    private readonly backgroundAssetFrame;
    private readonly backgroundAssetWidth;
    private readonly backgroundAssetHeight;
    private interactiveObject;
    private backgroundRectangle;
    private sprite;
    constructor(name: string, scene: Phaser.Scene, pluginManager: Phaser.Plugins.PluginManager, opts?: ButtonOptions);
    destroy(): void;
    /**
     * Update button text
     * @since 1.0.0
     * @author Antoine POUS
     * @param text
     * @return void
     */
    updateText(text: string): void;
    /**
     * Set button text
     * @since 1.0.0
     * @author Antoine POUS
     * @param text
     * @param opts
     * @return void
     */
    setText(text: string, opts?: ButtonTextOptions): void;
    /**
     * Remove interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    removeInteractive(): void;
    /**
     * Disable interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    disableInteractive(): void;
    /**
     * Enable interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    setInteractive(): void;
    /**
     * Click the button
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    click(): void;
    /**
     * Enable the debug in console log for the button
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    enableDebug(): void;
    private _createButton;
    /**
     * Create button background
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    private _createBackground;
    /**
     * Creates the border of the button
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    private _createBorder;
    /**
     * Listen for interactions
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    private _on;
}
export { Button };
