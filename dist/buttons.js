"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const buttonOptions_1 = require("./buttonOptions");
class Button {
    constructor(name, scene, pluginManager, opts) {
        this.name = name;
        this.scene = scene;
        this.pluginManager = pluginManager;
        this.debug = false;
        this.onClick = undefined;
        this.onPointerover = undefined;
        this.onPointerout = undefined;
        this.x = 5;
        this.y = 5;
        this.text = undefined;
        this.height = 32;
        this.width = 32;
        this.borderWidth = 0;
        this.borderColor = 0xff9814;
        this.borderAlpha = 1;
        this.hoverColor = undefined;
        this.backgroundColor = 0x459042;
        this.backgroundAssetKey = undefined;
        this.backgroundAssetPlay = undefined;
        this.backgroundAssetFrame = undefined;
        this.backgroundAssetWidth = undefined;
        this.backgroundAssetHeight = undefined;
        if (!opts)
            opts = {};
        if (opts.x)
            this.x = opts.x;
        if (opts.y)
            this.y = opts.y;
        if (opts.onClick)
            this.onClick = opts.onClick;
        if (opts.width)
            this.width = opts.width;
        if (opts.height)
            this.height = opts.height;
        if (opts.borderWidth)
            this.borderWidth = opts.borderWidth;
        if (opts.borderColor !== undefined)
            this.borderColor = opts.borderColor;
        if (opts.borderAlpha)
            this.borderAlpha = opts.borderAlpha;
        if (opts.backgroundColor !== undefined)
            this.backgroundColor = opts.backgroundColor;
        if (opts.backgroundAssetKey)
            this.backgroundAssetKey = opts.backgroundAssetKey;
        if (opts.backgroundAssetFrame === 0 || opts.backgroundAssetFrame)
            this.backgroundAssetFrame = opts.backgroundAssetFrame;
        if (opts.backgroundAssetPlay)
            this.backgroundAssetPlay = opts.backgroundAssetPlay;
        if (opts.backgroundAssetWidth)
            this.backgroundAssetWidth = opts.backgroundAssetWidth;
        if (opts.backgroundAssetHeight)
            this.backgroundAssetHeight = opts.backgroundAssetHeight;
        if (opts.hoverColor !== undefined)
            this.hoverColor = opts.hoverColor;
        this._createButton();
    }
    destroy() {
        if (this.backgroundRectangle)
            this.backgroundRectangle.destroy();
        if (this.interactiveObject)
            this.interactiveObject.destroy();
    }
    /**
     * Update button text
     * @since 1.0.0
     * @author Antoine POUS
     * @param text
     * @return void
     */
    updateText(text) {
        if (this.text)
            this.text.setText(text);
        else
            this.setText(text);
    }
    /**
     * Set button text
     * @since 1.0.0
     * @author Antoine POUS
     * @param text
     * @param opts
     * @return void
     */
    setText(text, opts) {
        if (!opts)
            opts = {};
        this.text = this.scene.make.text({ text });
        if (opts.color)
            this.text.setColor(opts.color);
        this.text.setY(this.y);
        this.text.setX(this.x);
        this.text.setFixedSize(this.width, this.height);
        if (!opts)
            opts = {};
        if (!opts.fontSize)
            opts.fontSize = 12;
        if (opts.fontSize)
            this.text.setFontSize(opts.fontSize);
        if (!opts.alignX)
            opts.alignX = buttonOptions_1.ButtonTextAlignX.center;
        if (!opts.alignY)
            opts.alignY = buttonOptions_1.ButtonTextAlignY.middle;
        let paddingLeft = 0;
        let paddingTop = 0;
        let paddingRight = 0;
        let paddingBottom = 0;
        const textWidth = this.text.context.measureText(`${text}`).width;
        if (opts.alignY === buttonOptions_1.ButtonTextAlignY.top) {
            paddingTop = 0;
            paddingBottom = this.height - opts.fontSize;
        }
        if (opts.alignY === buttonOptions_1.ButtonTextAlignY.middle) {
            const padding = (this.height / 2) - (opts.fontSize / 2);
            paddingTop = padding;
            paddingBottom = padding;
        }
        if (opts.alignY === buttonOptions_1.ButtonTextAlignY.bottom) {
            paddingTop = this.height - opts.fontSize;
            paddingBottom = 0;
        }
        if (opts.alignX === buttonOptions_1.ButtonTextAlignX.left) {
            paddingLeft = 0;
            paddingRight = this.width;
        }
        if (opts.alignX === buttonOptions_1.ButtonTextAlignX.center) {
            paddingLeft = (this.width / 2) - (textWidth / 2);
            paddingRight = (this.width / 2) - (textWidth / 2);
        }
        if (opts.alignX === buttonOptions_1.ButtonTextAlignX.right) {
            paddingLeft = this.width;
            paddingRight = 0;
        }
        this.text.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }
    /**
     * Remove interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    removeInteractive() {
        if (this.interactiveObject)
            this.interactiveObject.removeInteractive();
    }
    /**
     * Disable interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    disableInteractive() {
        if (this.interactiveObject)
            this.interactiveObject.disableInteractive();
    }
    /**
     * Enable interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    setInteractive() {
        if (!this.interactiveObject) {
            this.interactiveObject = this.scene.add.rectangle(this.x + (this.width / 2), this.y + (this.height / 2), this.width, this.height);
            this._on();
        }
        // TODO add border width to hitarea
        this.interactiveObject.setInteractive();
    }
    /**
     * Click the button
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    click() {
        if (this.onClick)
            this.onClick();
    }
    /**
     * Enable the debug in console log for the button
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    enableDebug() {
        this.debug = true;
    }
    _createButton() {
        this._createBackground();
        this._createBorder();
    }
    /**
     * Create button background
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    _createBackground() {
        this.backgroundRectangle = this.scene.add.rectangle(this.x + (this.width / 2), this.y + (this.height / 2), this.width - 1, this.height - 1);
        this.backgroundRectangle.setFillStyle(this.backgroundColor, 1);
        if (this.backgroundAssetKey) {
            this.sprite = this.scene.add.sprite(this.x + (this.width / 2), this.y + (this.height / 2), this.backgroundAssetKey);
            this.sprite.setDisplaySize(this.backgroundAssetWidth || this.width - (this.borderWidth * 2), this.backgroundAssetHeight || this.height - (this.borderWidth * 2));
            this.sprite.setSize(this.backgroundAssetWidth || this.width - (this.borderWidth * 2), this.backgroundAssetHeight || this.height - (this.borderWidth * 2));
            this.sprite.setActive(true);
            if (this.backgroundAssetFrame === 0 || this.backgroundAssetFrame)
                this.sprite.setFrame(this.backgroundAssetFrame);
            if (this.backgroundAssetPlay)
                this.sprite.play(this.backgroundAssetPlay);
        }
    }
    /**
     * Creates the border of the button
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    _createBorder() {
        if (!this.borderColor || this.borderWidth <= 0 || this.borderAlpha <= 0)
            return;
        if (this.backgroundRectangle)
            this.backgroundRectangle.setStrokeStyle(this.borderWidth, this.borderColor, this.borderAlpha);
        // TODO if opts borderRounded
    }
    /**
     * Listen for interactions
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    _on() {
        if (!this.interactiveObject)
            return;
        this.interactiveObject.on('pointerover', () => {
            if (this.debug)
                console.log(`pointerover ${this.name}`);
            if (this.hoverColor !== undefined && this.sprite)
                this.sprite.setTint(this.hoverColor);
            if (this.hoverColor !== undefined && !this.sprite) {
                if (this.backgroundRectangle)
                    this.backgroundRectangle.setFillStyle(this.hoverColor, 1);
            }
            if (this.onPointerover && typeof this.onPointerover === "function")
                this.onPointerover();
        });
        this.interactiveObject.on("pointerout", () => {
            if (this.debug)
                console.log(`pointerout ${this.name}`);
            if (this.hoverColor !== undefined && this.sprite)
                this.sprite.clearTint();
            if (this.hoverColor !== undefined && !this.sprite) {
                if (this.backgroundRectangle)
                    this.backgroundRectangle.setFillStyle(this.backgroundColor, 1);
            }
            if (this.onPointerout && typeof this.onPointerout === "function")
                this.onPointerout();
        });
        this.interactiveObject.on('pointerup', () => {
            if (this.debug)
                console.log(`click ${this.name}`);
            this.onClick && typeof this.onClick === "function" ? this.onClick() : null;
        });
    }
}
exports.Button = Button;
//# sourceMappingURL=buttons.js.map