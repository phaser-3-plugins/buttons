"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonTextAlignY;
(function (ButtonTextAlignY) {
    ButtonTextAlignY["top"] = "top";
    ButtonTextAlignY["middle"] = "middle";
    ButtonTextAlignY["bottom"] = "bottom";
})(ButtonTextAlignY || (ButtonTextAlignY = {}));
exports.ButtonTextAlignY = ButtonTextAlignY;
var ButtonTextAlignX;
(function (ButtonTextAlignX) {
    ButtonTextAlignX["left"] = "left";
    ButtonTextAlignX["center"] = "center";
    ButtonTextAlignX["right"] = "right";
})(ButtonTextAlignX || (ButtonTextAlignX = {}));
exports.ButtonTextAlignX = ButtonTextAlignX;
//# sourceMappingURL=buttonOptions.js.map