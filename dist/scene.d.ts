import * as Phaser from "phaser";
import { Button } from "./buttons";
import { ButtonOptions } from "./buttonOptions";
declare class ButtonScenePlugin extends Phaser.Plugins.ScenePlugin {
    private store;
    constructor(scene: Phaser.Scene, pluginManager: Phaser.Plugins.PluginManager);
    boot(): void;
    destroy(): void;
    create(name: string, opts?: ButtonOptions): Button;
    get(name: string): Button | undefined;
}
export { ButtonScenePlugin };
