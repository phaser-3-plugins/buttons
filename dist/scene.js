"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Phaser = __importStar(require("phaser"));
const buttons_1 = require("./buttons");
// @ts-ignore
class Collection extends Map {
    constructor() {
        return new Map();
        super();
    }
}
class ButtonScenePlugin extends Phaser.Plugins.ScenePlugin {
    constructor(scene, pluginManager) {
        super(scene, pluginManager);
        this.store = new Collection();
    }
    boot() {
        this.scene.sys.events.on('destroy', this.destroy, this);
    }
    destroy() {
        this.store.forEach((btn) => {
            btn.destroy();
            this.store.delete(btn.name);
        });
    }
    create(name, opts) {
        const btn = new buttons_1.Button(name, this.scene, this.pluginManager, opts);
        this.store.set(name, btn);
        return btn;
    }
    get(name) {
        return this.store.get(name);
    }
}
exports.ButtonScenePlugin = ButtonScenePlugin;
//# sourceMappingURL=scene.js.map