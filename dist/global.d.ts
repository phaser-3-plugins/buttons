import * as Phaser from "phaser";
import { Button } from "./buttons";
import { ButtonOptions } from "./buttonOptions";
declare class ButtonScenePlugin extends Phaser.Plugins.BasePlugin {
    private store;
    constructor(pluginManager: Phaser.Plugins.PluginManager);
    destroy(): void;
    create(name: string, opts?: ButtonOptions): Button;
    get(name: string): Button | undefined;
}
export { ButtonScenePlugin };
