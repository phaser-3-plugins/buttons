# Phaser 3 Buttons scene plugin
This plugin is designed to help you to create easily buttons in your game.

**Important note:** This plugin is a work in progress, i mostly implement the features i need for my projects. Feel free to improve it and made PR.

## Installation
To install this package use `npm install @phaser3/plugin-buttons`

## Phaser 3 configuration
This plugin is available for both Global and Scene scopes. You have to target the one you want use and declare it into your Phaser 3 plugins configuration.

```typescript
import {ButtonGlobalPlugin} "@phaser3/plugin-buttons/src/global";
import {ButtonScenePlugin} "@phaser3/plugin-buttons/src/scene";

plugins: {
    global: [
        {key: 'ButtonsPlugin', plugin: ButtonGlobalPlugin, mapping: 'button'}
    ],
    scene: [
        { key: 'ButtonsPlugin', plugin: ButtonsPlugin, mapping: 'button' },
    ]
},
```

## Basic usage
To create a button you must call `Phaser.Scenes.Systems.button`. Here is a small example with a pause button:

```typescript
import {Button, ButtonOptions} from "@phaser3/scene-plugin-buttons";
const buttonPause: Button = <Button>Phaser.Scenes.Systems.button.create("pause");

buttonPause.setInteractive();

buttonPause.onClick = () => {
	this.scene.pause();
};
```

This sample will create a basic button with default style and effects. When you click on it, the scene is paused.

## Advanced usage
```typescript
const buttonPause: Button = <Button>Phaser.Scenes.Systems.button.create("pause", <ButtonOptions>{});

buttonPause.setInteractive();

buttonPause.onClick = () => {
	this.scene.pause();
};
```

### Buttons options
| Name | Type | Default | Description |
|------|------|---------|-------------|
| x | Number | 5 | Position x
| y | Number | 5 | Position y
|--|||
| height | Number | 32 | Button height
| width | Number | 32 | Button width
|--|||
| borderWidth | Number | 0 | Button border width
| borderColor | Number | 0xff9814 | Button border color
| borderAlpha | Number | 1 | Border alpha channel
|--|||
| hoverColor | Number | Undefined | Button color while pointer is hover
|--|||
| BackgroundColor | Number | 0x459042 | Button background color
| backgroundAssetKey | String | Undefined | Asset key to use as background
| backgroundAssetFrame | Number | 0 | The first frame to display (if not animated this frame is displayed statically)
| backgroundAssetPlay | String | Undefined | Animation key
| backgroundAssetWidth | Number | Undefined | Asset width within the button
| backgroundAssetHeight | Number | Undefined | Asset height within the button
|--|||
| onClick | Function | Undefined | Callback called when the button is clicked

## Add a text to your button
You can add some text to your button.

```typescript
buttonPause.setText("pause", <ButtonTextOptions>{})
```

When you need you can update your button text easily, this will apply the same text options to the new text.
```typescript
buttonPause.updateText("resume");
```

### Button text options
| Name | Type | Default | Description |
|------|------|---------|-------------|
| fontSize | Number | 12 | Button text font size |
| alignX | ButtonTextAlignX | ButtonTextAlignX.center | Button text alignment |
| alignY | ButtonTextAlignY | ButtonTextAlignY.middle | Button vertical text alignment |
| color | String | #FFFFFF | Button text color

* `ButtonTextAlignX` keys are `left`, `center`, `right`
* `ButtonTextAlignY` keys are `top`, `middle`, `bottom`
