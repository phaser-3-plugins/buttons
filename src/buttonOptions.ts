interface ButtonOptions {
    x?: number,
    y?: number,

    height?: number,
    width?: number,

    borderWidth?: number,
    borderColor?: number,
    borderAlpha?: number,

    hoverColor?: number,

    backgroundColor?: number,
    backgroundAssetKey?: string,
    backgroundAssetFrame?: number,
    backgroundAssetPlay?: string,
    backgroundAssetWidth?: number,
    backgroundAssetHeight?: number,

    cooldownDuration?: number,

    onClick?: Function
}

enum ButtonTextAlignY {
    top    = "top",
    middle = "middle", // Default
    bottom = "bottom",
}

enum ButtonTextAlignX {
    left   = "left",
    center = "center", // Default
    right  = "right",
}

interface ButtonTextOptions {
    fontSize?: number,
    alignX?: ButtonTextAlignX,
    alignY?: ButtonTextAlignY,
    color?: string,
}

export {ButtonOptions, ButtonTextAlignY, ButtonTextAlignX, ButtonTextOptions}
