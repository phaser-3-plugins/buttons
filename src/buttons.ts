import * as Phaser from "phaser";
import {ButtonOptions, ButtonTextAlignX, ButtonTextAlignY, ButtonTextOptions} from "./buttonOptions";

class Button {

    private debug: boolean = false;

    public onClick: Function | undefined = undefined;
    public onPointerover: Function | undefined = undefined;
    public onPointerout: Function | undefined = undefined;

    private readonly x: number = 5;
    private readonly y: number = 5;

    private text: Phaser.GameObjects.Text | undefined = undefined;

    private readonly height: number = 32;
    private readonly width: number = 32;

    private readonly borderWidth: number = 0;
    private readonly borderColor: number = 0xff9814;
    private readonly borderAlpha: number = 1;

    private readonly hoverColor: number | undefined = undefined;

    private readonly backgroundColor: number = 0x459042;
    private readonly backgroundAssetKey: string | undefined = undefined;
    private readonly backgroundAssetPlay: string | undefined = undefined;
    private readonly backgroundAssetFrame: number | undefined = undefined;
    private readonly backgroundAssetWidth: number | undefined = undefined;
    private readonly backgroundAssetHeight: number | undefined = undefined;

    private interactiveObject: Phaser.GameObjects.Rectangle | undefined;
    private backgroundRectangle: Phaser.GameObjects.Rectangle | undefined;

    private sprite: Phaser.GameObjects.Sprite | undefined;

    constructor(public name: string, public scene: Phaser.Scene, public pluginManager: Phaser.Plugins.PluginManager, opts?: ButtonOptions) {

        if(!opts)
            opts = {};

        if(opts.x)
            this.x = opts.x;

        if(opts.y)
            this.y = opts.y;

        if(opts.onClick)
            this.onClick = opts.onClick;

        if(opts.width)
            this.width = opts.width;

        if(opts.height)
            this.height = opts.height;

        if(opts.borderWidth)
            this.borderWidth = opts.borderWidth;

        if(opts.borderColor !== undefined)
            this.borderColor = opts.borderColor;

        if(opts.borderAlpha)
            this.borderAlpha = opts.borderAlpha;

        if(opts.backgroundColor !== undefined)
            this.backgroundColor = opts.backgroundColor;

        if(opts.backgroundAssetKey)
            this.backgroundAssetKey = opts.backgroundAssetKey;

        if(opts.backgroundAssetFrame === 0 || opts.backgroundAssetFrame)
            this.backgroundAssetFrame = opts.backgroundAssetFrame;

        if(opts.backgroundAssetPlay)
            this.backgroundAssetPlay = opts.backgroundAssetPlay;

        if(opts.backgroundAssetWidth)
            this.backgroundAssetWidth = opts.backgroundAssetWidth;

        if(opts.backgroundAssetHeight)
            this.backgroundAssetHeight = opts.backgroundAssetHeight;

        if(opts.hoverColor !== undefined)
            this.hoverColor = opts.hoverColor;

        this._createButton();
    }

    destroy() {
        if(this.backgroundRectangle)
            this.backgroundRectangle.destroy();

        if(this.interactiveObject)
            this.interactiveObject.destroy();
    }

    /**
     * Update button text
     * @since 1.0.0
     * @author Antoine POUS
     * @param text
     * @return void
     */
    public updateText(text: string): void {
        if(this.text)
            this.text.setText(text);
        else
            this.setText(text);
    }

    /**
     * Set button text
     * @since 1.0.0
     * @author Antoine POUS
     * @param text
     * @param opts
     * @return void
     */
    public setText(text: string, opts?: ButtonTextOptions): void {

        if(!opts) opts = {};

        this.text = this.scene.make.text({text});

        if(opts.color)
            this.text.setColor(opts.color);

        this.text.setY(this.y);
        this.text.setX(this.x);

        this.text.setFixedSize(this.width, this.height);

        if(!opts) opts = {};

        if(!opts.fontSize)
            opts.fontSize = 12;

        if(opts.fontSize)
            this.text.setFontSize(opts.fontSize);

        if(!opts.alignX)
            opts.alignX = ButtonTextAlignX.center;

        if(!opts.alignY)
            opts.alignY = ButtonTextAlignY.middle;

        let paddingLeft: number = 0;
        let paddingTop: number = 0;
        let paddingRight: number = 0;
        let paddingBottom: number = 0;

        const textWidth: number = this.text.context.measureText(`${text}`).width;

        if(opts.alignY === ButtonTextAlignY.top) {
            paddingTop = 0;
            paddingBottom = this.height - opts.fontSize;
        }

        if(opts.alignY === ButtonTextAlignY.middle) {
            const padding: number = (this.height / 2) - (opts.fontSize / 2);
            paddingTop = padding;
            paddingBottom = padding;
        }

        if(opts.alignY === ButtonTextAlignY.bottom) {
            paddingTop = this.height - opts.fontSize;
            paddingBottom = 0;
        }

        if(opts.alignX === ButtonTextAlignX.left) {
            paddingLeft = 0;
            paddingRight = this.width;
        }

        if(opts.alignX === ButtonTextAlignX.center) {
            paddingLeft = (this.width / 2) - (textWidth / 2);
            paddingRight = (this.width / 2) - (textWidth / 2);
        }

        if(opts.alignX === ButtonTextAlignX.right) {
            paddingLeft = this.width;
            paddingRight = 0;
        }

        this.text.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    /**
     * Remove interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    public removeInteractive(): void {
        if(this.interactiveObject)
            this.interactiveObject.removeInteractive();
    }

    /**
     * Disable interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    public disableInteractive(): void {
        if(this.interactiveObject)
            this.interactiveObject.disableInteractive();
    }

    /**
     * Enable interactive mode
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    public setInteractive(): void {
        if(!this.interactiveObject) {
            this.interactiveObject = this.scene.add.rectangle(this.x + (this.width / 2), this.y + (this.height / 2), this.width, this.height);
            this._on();
        }

        // TODO add border width to hitarea
        this.interactiveObject.setInteractive();
    }

    /**
     * Click the button
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    public click(): void {
        if(this.onClick)
           this.onClick();
    }

    /**
     * Enable the debug in console log for the button
     * @since 1.0.0
     * @author Antoine POUS
     * @return void
     */
    public enableDebug(): void {
        this.debug = true;
    }

    private _createButton(): void {
        this._createBackground();
        this._createBorder();
    }

    /**
     * Create button background
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    private _createBackground (): void {
        this.backgroundRectangle = this.scene.add.rectangle(this.x + (this.width / 2), this.y + (this.height / 2), this.width - 1, this.height - 1);
        this.backgroundRectangle.setFillStyle(this.backgroundColor, 1);
        if(this.backgroundAssetKey) {
            this.sprite = this.scene.add.sprite(this.x + (this.width / 2), this.y + (this.height / 2), this.backgroundAssetKey);
            this.sprite.setDisplaySize(this.backgroundAssetWidth || this.width - (this.borderWidth * 2), this.backgroundAssetHeight || this.height - (this.borderWidth * 2));
            this.sprite.setSize(this.backgroundAssetWidth || this.width - (this.borderWidth * 2), this.backgroundAssetHeight || this.height  - (this.borderWidth * 2));
            this.sprite.setActive(true);

            if(this.backgroundAssetFrame === 0 || this.backgroundAssetFrame)
                this.sprite.setFrame(this.backgroundAssetFrame);

            if(this.backgroundAssetPlay)
                this.sprite.play(this.backgroundAssetPlay);
        }
    }

    /**
     * Creates the border of the button
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    private _createBorder(): void {
        if(!this.borderColor || this.borderWidth <= 0 || this.borderAlpha <= 0)
            return;

        if(this.backgroundRectangle)
            this.backgroundRectangle.setStrokeStyle(this.borderWidth, this.borderColor, this.borderAlpha)

        // TODO if opts borderRounded
    }

    /**
     * Listen for interactions
     * @since 1.0.0
     * @author Antoine POUS
     * @private
     * @return void
     */
    private _on(): void {

        if(!this.interactiveObject)
            return;

        this.interactiveObject.on('pointerover', () => {

            if(this.debug)
                console.log(`pointerover ${this.name}`);

            if(this.hoverColor !== undefined && this.sprite)
                this.sprite.setTint(this.hoverColor);

            if(this.hoverColor !== undefined && !this.sprite) {

                if(this.backgroundRectangle)
                    this.backgroundRectangle.setFillStyle(this.hoverColor, 1);
            }

            if(this.onPointerover && typeof this.onPointerover === "function")
                this.onPointerover();
        });

        this.interactiveObject.on("pointerout", () => {

            if(this.debug)
                console.log(`pointerout ${this.name}`);

            if(this.hoverColor !== undefined && this.sprite)
                this.sprite.clearTint();

            if(this.hoverColor !== undefined && !this.sprite) {

                if(this.backgroundRectangle)
                    this.backgroundRectangle.setFillStyle(this.backgroundColor, 1);
            }

            if(this.onPointerout && typeof this.onPointerout === "function")
                this.onPointerout();
        });

        this.interactiveObject.on('pointerup', () => {

            if(this.debug)
                console.log(`click ${this.name}`);

            this.onClick && typeof this.onClick === "function"  ? this.onClick() : null
        });
    }

}

export {Button}
