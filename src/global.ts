import * as Phaser from "phaser";
import {Button} from "./buttons";
import {ButtonOptions} from "./buttonOptions";

// @ts-ignore
class Collection<String, T> extends Map {

    constructor() {
        return new Map();
        super();
    }
}

class ButtonScenePlugin extends Phaser.Plugins.BasePlugin {

    private store: Collection<string, Button> = new Collection<string, Button>();

    constructor(pluginManager: Phaser.Plugins.PluginManager) {
        super(pluginManager)
    }

    public destroy() {
        this.store.forEach((btn: Button) => {
            btn.destroy();
            this.store.delete(btn.name);
        });
    }

    public create(name: string, opts?: ButtonOptions): Button {
        const btn: Button = new Button(name, this.scene, this.pluginManager, opts);
        this.store.set(name, btn);
        return btn;
    }

    public get(name: string): Button|undefined {
        return this.store.get(name);
    }

}

export {ButtonScenePlugin}
